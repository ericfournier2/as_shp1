require(csaw)
require(edgeR)
require(dplyr)

readset = read.table("raw/readset.txt", sep="\t", header=TRUE, stringsAsFactors=FALSE)
readset = readset[!duplicated(readset$Sample),]
bam_filenames = paste0(readset$Sample, ".sorted.dup.bam")
readset$BAM = file.path("output/pipeline/alignment/", readset$Sample, bam_filenames)

# Load data
OUTDIR="output/csaw"
dir.create(OUTDIR)

data <- windowCounts(readset$BAM, ext=200, width=10)

# 2. Filtering out uninteresting regions.
keep <- aveLogCPM(asDGEList(data)) >= -1
data <- data[keep,]

# 3. Calculating normalization factors.
binned <- windowCounts(readset$BAM, bin=TRUE, width=10000)
normfacs <- normOffsets(binned)

# 4. Identifying DB windows.
y <- asDGEList(data, norm.factors=colData(normfacs)$norm.factors)

for(antibody in unique(readset$Antibody)) {
    indices = readset$Antibody == antibody
    design_subset = readset[indices,]
    dge_subset = y[,indices]
    
    strain = factor(design_subset$Strain, levels=c("WT", "SHP1-KO"))
    design = model.matrix(~strain)

    #dge.disp <- estimateDisp(dge_subset, design)
    fit <- glmFit(dge_subset, design, robust=TRUE, dispersion=0.05)
    results <- glmLRT(fit)

    merged <- mergeWindows(rowRanges(data), tol=1000L)
    tabcom <- combineTests(merged$id, results$table)
    
    write.table(cbind(as.data.frame(merged$region), tabcom)[tabcom$FDR<0.05,], 
                file.path(OUTDIR, paste0(antibody, ".txt")),
                sep="\t", col.names=TRUE, row.names=FALSE, quote=FALSE)
}
