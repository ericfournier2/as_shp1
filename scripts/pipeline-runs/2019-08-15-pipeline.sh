#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# ChipSeq SlurmScheduler Job Submission Bash script
# Version: 3.1.4-beta
# Created on: 2019-08-15T08:43:21
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 0 job... skipping
#   merge_trimmomatic_stats: 0 job... skipping
#   bwa_mem_picard_sort_sam: 0 job... skipping
#   samtools_view_filter: 0 job... skipping
#   picard_merge_sam_files: 0 job... skipping
#   picard_mark_duplicates: 0 job... skipping
#   macs2_callpeak: 25 jobs
#   TOTAL: 25 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/scratch/efournie/am_shp1/output/pipeline
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/ChipSeq_job_list_$TIMESTAMP
export CONFIG_FILES="/cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.4/pipelines/chipseq/chipseq.base.ini,/cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.4/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini"
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: macs2_callpeak
#-------------------------------------------------------------------------------
STEP=macs2_callpeak
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_1_JOB_ID: macs2_callpeak.SHP1_WT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.SHP1_WT
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.SHP1_WT.c79b5f758ee1c17dbe5f0308e91ae4df.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.SHP1_WT.c79b5f758ee1c17dbe5f0308e91ae4df.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/SHP1_WT && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/SHP1_WT/SHP1_WT.sorted.dup.bam \
  --control \
  alignment/WCE_WT/WCE_WT.sorted.dup.bam \
  --name peak_call/SHP1_WT/SHP1_WT \
  >& peak_call/SHP1_WT/SHP1_WT.diag.macs.out
macs2_callpeak.SHP1_WT.c79b5f758ee1c17dbe5f0308e91ae4df.mugqic.done
)
macs2_callpeak_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_2_JOB_ID: macs2_callpeak_bigBed.SHP1_WT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.SHP1_WT
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.SHP1_WT.43b299668157069c30b09d68d30fa17a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.SHP1_WT.43b299668157069c30b09d68d30fa17a.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/SHP1_WT/SHP1_WT_peaks.broadPeak > peak_call/SHP1_WT/SHP1_WT_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/SHP1_WT/SHP1_WT_peaks.broadPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/SHP1_WT/SHP1_WT_peaks.broadPeak.bb
macs2_callpeak_bigBed.SHP1_WT.43b299668157069c30b09d68d30fa17a.mugqic.done
)
macs2_callpeak_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_3_JOB_ID: macs2_callpeak.SHP1_SHP1-KO
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.SHP1_SHP1-KO
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.SHP1_SHP1-KO.83c9559ea0820db27c28c8644a960c82.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.SHP1_SHP1-KO.83c9559ea0820db27c28c8644a960c82.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/SHP1_SHP1-KO && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/SHP1_SHP1-KO/SHP1_SHP1-KO.sorted.dup.bam \
  --control \
  alignment/WCE_SHP1-KO/WCE_SHP1-KO.sorted.dup.bam \
  --name peak_call/SHP1_SHP1-KO/SHP1_SHP1-KO \
  >& peak_call/SHP1_SHP1-KO/SHP1_SHP1-KO.diag.macs.out
macs2_callpeak.SHP1_SHP1-KO.83c9559ea0820db27c28c8644a960c82.mugqic.done
)
macs2_callpeak_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_4_JOB_ID: macs2_callpeak_bigBed.SHP1_SHP1-KO
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.SHP1_SHP1-KO
JOB_DEPENDENCIES=$macs2_callpeak_3_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.SHP1_SHP1-KO.1b92d39c517fb8b0eb608f91481c1aab.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.SHP1_SHP1-KO.1b92d39c517fb8b0eb608f91481c1aab.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/SHP1_SHP1-KO/SHP1_SHP1-KO_peaks.broadPeak > peak_call/SHP1_SHP1-KO/SHP1_SHP1-KO_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/SHP1_SHP1-KO/SHP1_SHP1-KO_peaks.broadPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/SHP1_SHP1-KO/SHP1_SHP1-KO_peaks.broadPeak.bb
macs2_callpeak_bigBed.SHP1_SHP1-KO.1b92d39c517fb8b0eb608f91481c1aab.mugqic.done
)
macs2_callpeak_4_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_5_JOB_ID: macs2_callpeak.Rbp1_WT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Rbp1_WT
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Rbp1_WT.c21066d98f5f95f7646b474ea6959d4e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Rbp1_WT.c21066d98f5f95f7646b474ea6959d4e.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Rbp1_WT && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/Rbp1_WT/Rbp1_WT.sorted.dup.bam \
  --control \
  alignment/WCE_WT/WCE_WT.sorted.dup.bam \
  --name peak_call/Rbp1_WT/Rbp1_WT \
  >& peak_call/Rbp1_WT/Rbp1_WT.diag.macs.out
macs2_callpeak.Rbp1_WT.c21066d98f5f95f7646b474ea6959d4e.mugqic.done
)
macs2_callpeak_5_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_6_JOB_ID: macs2_callpeak_bigBed.Rbp1_WT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Rbp1_WT
JOB_DEPENDENCIES=$macs2_callpeak_5_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Rbp1_WT.ae003d1a1103b37d2cf8443ad9016233.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Rbp1_WT.ae003d1a1103b37d2cf8443ad9016233.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Rbp1_WT/Rbp1_WT_peaks.broadPeak > peak_call/Rbp1_WT/Rbp1_WT_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/Rbp1_WT/Rbp1_WT_peaks.broadPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/Rbp1_WT/Rbp1_WT_peaks.broadPeak.bb
macs2_callpeak_bigBed.Rbp1_WT.ae003d1a1103b37d2cf8443ad9016233.mugqic.done
)
macs2_callpeak_6_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_7_JOB_ID: macs2_callpeak.Rbp1_SHP1-KO
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Rbp1_SHP1-KO
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Rbp1_SHP1-KO.71bb41120b0f3b243d2ef8e9eb342a0a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Rbp1_SHP1-KO.71bb41120b0f3b243d2ef8e9eb342a0a.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Rbp1_SHP1-KO && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/Rbp1_SHP1-KO/Rbp1_SHP1-KO.sorted.dup.bam \
  --control \
  alignment/WCE_SHP1-KO/WCE_SHP1-KO.sorted.dup.bam \
  --name peak_call/Rbp1_SHP1-KO/Rbp1_SHP1-KO \
  >& peak_call/Rbp1_SHP1-KO/Rbp1_SHP1-KO.diag.macs.out
macs2_callpeak.Rbp1_SHP1-KO.71bb41120b0f3b243d2ef8e9eb342a0a.mugqic.done
)
macs2_callpeak_7_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_8_JOB_ID: macs2_callpeak_bigBed.Rbp1_SHP1-KO
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Rbp1_SHP1-KO
JOB_DEPENDENCIES=$macs2_callpeak_7_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Rbp1_SHP1-KO.176cc647249b1a2ff7e95d1559849868.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Rbp1_SHP1-KO.176cc647249b1a2ff7e95d1559849868.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Rbp1_SHP1-KO/Rbp1_SHP1-KO_peaks.broadPeak > peak_call/Rbp1_SHP1-KO/Rbp1_SHP1-KO_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/Rbp1_SHP1-KO/Rbp1_SHP1-KO_peaks.broadPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/Rbp1_SHP1-KO/Rbp1_SHP1-KO_peaks.broadPeak.bb
macs2_callpeak_bigBed.Rbp1_SHP1-KO.176cc647249b1a2ff7e95d1559849868.mugqic.done
)
macs2_callpeak_8_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_9_JOB_ID: macs2_callpeak.Rbp1-P-Ser2_WT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Rbp1-P-Ser2_WT
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Rbp1-P-Ser2_WT.fc744ec110a248b913feba09545fcb5d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Rbp1-P-Ser2_WT.fc744ec110a248b913feba09545fcb5d.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Rbp1-P-Ser2_WT && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT.sorted.dup.bam \
  --control \
  alignment/WCE_WT/WCE_WT.sorted.dup.bam \
  --name peak_call/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT \
  >& peak_call/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT.diag.macs.out
macs2_callpeak.Rbp1-P-Ser2_WT.fc744ec110a248b913feba09545fcb5d.mugqic.done
)
macs2_callpeak_9_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_10_JOB_ID: macs2_callpeak_bigBed.Rbp1-P-Ser2_WT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Rbp1-P-Ser2_WT
JOB_DEPENDENCIES=$macs2_callpeak_9_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Rbp1-P-Ser2_WT.4f7c2c42751305ccb38202178a27ab0e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Rbp1-P-Ser2_WT.4f7c2c42751305ccb38202178a27ab0e.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT_peaks.broadPeak > peak_call/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT_peaks.broadPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT_peaks.broadPeak.bb
macs2_callpeak_bigBed.Rbp1-P-Ser2_WT.4f7c2c42751305ccb38202178a27ab0e.mugqic.done
)
macs2_callpeak_10_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_11_JOB_ID: macs2_callpeak.Rbp1-P-Ser2_SHP1-KO
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Rbp1-P-Ser2_SHP1-KO
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Rbp1-P-Ser2_SHP1-KO.d3f11ebc3320af7bb8cedabde298bf6e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Rbp1-P-Ser2_SHP1-KO.d3f11ebc3320af7bb8cedabde298bf6e.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Rbp1-P-Ser2_SHP1-KO && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO.sorted.dup.bam \
  --control \
  alignment/WCE_SHP1-KO/WCE_SHP1-KO.sorted.dup.bam \
  --name peak_call/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO \
  >& peak_call/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO.diag.macs.out
macs2_callpeak.Rbp1-P-Ser2_SHP1-KO.d3f11ebc3320af7bb8cedabde298bf6e.mugqic.done
)
macs2_callpeak_11_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_12_JOB_ID: macs2_callpeak_bigBed.Rbp1-P-Ser2_SHP1-KO
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Rbp1-P-Ser2_SHP1-KO
JOB_DEPENDENCIES=$macs2_callpeak_11_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Rbp1-P-Ser2_SHP1-KO.1c2ffb0eb4e08e00d620ff8a33860e62.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Rbp1-P-Ser2_SHP1-KO.1c2ffb0eb4e08e00d620ff8a33860e62.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO_peaks.broadPeak > peak_call/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO_peaks.broadPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO_peaks.broadPeak.bb
macs2_callpeak_bigBed.Rbp1-P-Ser2_SHP1-KO.1c2ffb0eb4e08e00d620ff8a33860e62.mugqic.done
)
macs2_callpeak_12_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_13_JOB_ID: macs2_callpeak.SHP1_WT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.SHP1_WT
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.SHP1_WT.4c346c6695feb08c09622e99301a71b7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.SHP1_WT.4c346c6695feb08c09622e99301a71b7.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/SHP1_WT && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/SHP1_WT/SHP1_WT.sorted.dup.bam \
  --control \
  alignment/WCE_WT/WCE_WT.sorted.dup.bam \
  --name peak_call/SHP1_WT/SHP1_WT \
  >& peak_call/SHP1_WT/SHP1_WT.diag.macs.out
macs2_callpeak.SHP1_WT.4c346c6695feb08c09622e99301a71b7.mugqic.done
)
macs2_callpeak_13_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_14_JOB_ID: macs2_callpeak_bigBed.SHP1_WT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.SHP1_WT
JOB_DEPENDENCIES=$macs2_callpeak_13_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.SHP1_WT.905c6285833d856f3fdc61db29f23d08.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.SHP1_WT.905c6285833d856f3fdc61db29f23d08.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/SHP1_WT/SHP1_WT_peaks.narrowPeak > peak_call/SHP1_WT/SHP1_WT_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/SHP1_WT/SHP1_WT_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/SHP1_WT/SHP1_WT_peaks.narrowPeak.bb
macs2_callpeak_bigBed.SHP1_WT.905c6285833d856f3fdc61db29f23d08.mugqic.done
)
macs2_callpeak_14_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_15_JOB_ID: macs2_callpeak.SHP1_SHP1-KO
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.SHP1_SHP1-KO
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.SHP1_SHP1-KO.e4a93b0ff6c6da6be74ef54fbc1e0724.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.SHP1_SHP1-KO.e4a93b0ff6c6da6be74ef54fbc1e0724.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/SHP1_SHP1-KO && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/SHP1_SHP1-KO/SHP1_SHP1-KO.sorted.dup.bam \
  --control \
  alignment/WCE_SHP1-KO/WCE_SHP1-KO.sorted.dup.bam \
  --name peak_call/SHP1_SHP1-KO/SHP1_SHP1-KO \
  >& peak_call/SHP1_SHP1-KO/SHP1_SHP1-KO.diag.macs.out
macs2_callpeak.SHP1_SHP1-KO.e4a93b0ff6c6da6be74ef54fbc1e0724.mugqic.done
)
macs2_callpeak_15_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_16_JOB_ID: macs2_callpeak_bigBed.SHP1_SHP1-KO
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.SHP1_SHP1-KO
JOB_DEPENDENCIES=$macs2_callpeak_15_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.SHP1_SHP1-KO.5d7675ea0deb2d9a39971a879aa96859.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.SHP1_SHP1-KO.5d7675ea0deb2d9a39971a879aa96859.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/SHP1_SHP1-KO/SHP1_SHP1-KO_peaks.narrowPeak > peak_call/SHP1_SHP1-KO/SHP1_SHP1-KO_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/SHP1_SHP1-KO/SHP1_SHP1-KO_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/SHP1_SHP1-KO/SHP1_SHP1-KO_peaks.narrowPeak.bb
macs2_callpeak_bigBed.SHP1_SHP1-KO.5d7675ea0deb2d9a39971a879aa96859.mugqic.done
)
macs2_callpeak_16_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_17_JOB_ID: macs2_callpeak.Rbp1_WT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Rbp1_WT
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Rbp1_WT.7d90d0ca1f1466782c74de4d5279984a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Rbp1_WT.7d90d0ca1f1466782c74de4d5279984a.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Rbp1_WT && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/Rbp1_WT/Rbp1_WT.sorted.dup.bam \
  --control \
  alignment/WCE_WT/WCE_WT.sorted.dup.bam \
  --name peak_call/Rbp1_WT/Rbp1_WT \
  >& peak_call/Rbp1_WT/Rbp1_WT.diag.macs.out
macs2_callpeak.Rbp1_WT.7d90d0ca1f1466782c74de4d5279984a.mugqic.done
)
macs2_callpeak_17_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_18_JOB_ID: macs2_callpeak_bigBed.Rbp1_WT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Rbp1_WT
JOB_DEPENDENCIES=$macs2_callpeak_17_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Rbp1_WT.f8e82d11498789cb54b810a2c1040ed4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Rbp1_WT.f8e82d11498789cb54b810a2c1040ed4.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Rbp1_WT/Rbp1_WT_peaks.narrowPeak > peak_call/Rbp1_WT/Rbp1_WT_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Rbp1_WT/Rbp1_WT_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/Rbp1_WT/Rbp1_WT_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Rbp1_WT.f8e82d11498789cb54b810a2c1040ed4.mugqic.done
)
macs2_callpeak_18_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_19_JOB_ID: macs2_callpeak.Rbp1_SHP1-KO
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Rbp1_SHP1-KO
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Rbp1_SHP1-KO.7e2cab0a7305b0ffa45f4b6249c4791b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Rbp1_SHP1-KO.7e2cab0a7305b0ffa45f4b6249c4791b.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Rbp1_SHP1-KO && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/Rbp1_SHP1-KO/Rbp1_SHP1-KO.sorted.dup.bam \
  --control \
  alignment/WCE_SHP1-KO/WCE_SHP1-KO.sorted.dup.bam \
  --name peak_call/Rbp1_SHP1-KO/Rbp1_SHP1-KO \
  >& peak_call/Rbp1_SHP1-KO/Rbp1_SHP1-KO.diag.macs.out
macs2_callpeak.Rbp1_SHP1-KO.7e2cab0a7305b0ffa45f4b6249c4791b.mugqic.done
)
macs2_callpeak_19_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_20_JOB_ID: macs2_callpeak_bigBed.Rbp1_SHP1-KO
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Rbp1_SHP1-KO
JOB_DEPENDENCIES=$macs2_callpeak_19_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Rbp1_SHP1-KO.cc8db4e134d5e4409dec2bbffc9dd51b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Rbp1_SHP1-KO.cc8db4e134d5e4409dec2bbffc9dd51b.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Rbp1_SHP1-KO/Rbp1_SHP1-KO_peaks.narrowPeak > peak_call/Rbp1_SHP1-KO/Rbp1_SHP1-KO_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Rbp1_SHP1-KO/Rbp1_SHP1-KO_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/Rbp1_SHP1-KO/Rbp1_SHP1-KO_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Rbp1_SHP1-KO.cc8db4e134d5e4409dec2bbffc9dd51b.mugqic.done
)
macs2_callpeak_20_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_21_JOB_ID: macs2_callpeak.Rbp1-P-Ser2_WT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Rbp1-P-Ser2_WT
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Rbp1-P-Ser2_WT.654fb4663406b48c6b3dfb589dcb8621.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Rbp1-P-Ser2_WT.654fb4663406b48c6b3dfb589dcb8621.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Rbp1-P-Ser2_WT && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT.sorted.dup.bam \
  --control \
  alignment/WCE_WT/WCE_WT.sorted.dup.bam \
  --name peak_call/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT \
  >& peak_call/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT.diag.macs.out
macs2_callpeak.Rbp1-P-Ser2_WT.654fb4663406b48c6b3dfb589dcb8621.mugqic.done
)
macs2_callpeak_21_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_22_JOB_ID: macs2_callpeak_bigBed.Rbp1-P-Ser2_WT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Rbp1-P-Ser2_WT
JOB_DEPENDENCIES=$macs2_callpeak_21_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Rbp1-P-Ser2_WT.8108593d52b62427a503d4650c474f02.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Rbp1-P-Ser2_WT.8108593d52b62427a503d4650c474f02.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT_peaks.narrowPeak > peak_call/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/Rbp1-P-Ser2_WT/Rbp1-P-Ser2_WT_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Rbp1-P-Ser2_WT.8108593d52b62427a503d4650c474f02.mugqic.done
)
macs2_callpeak_22_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_23_JOB_ID: macs2_callpeak.Rbp1-P-Ser2_SHP1-KO
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Rbp1-P-Ser2_SHP1-KO
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Rbp1-P-Ser2_SHP1-KO.7026d54efdc8fc105fedf98d65e173d8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Rbp1-P-Ser2_SHP1-KO.7026d54efdc8fc105fedf98d65e173d8.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Rbp1-P-Ser2_SHP1-KO && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO.sorted.dup.bam \
  --control \
  alignment/WCE_SHP1-KO/WCE_SHP1-KO.sorted.dup.bam \
  --name peak_call/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO \
  >& peak_call/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO.diag.macs.out
macs2_callpeak.Rbp1-P-Ser2_SHP1-KO.7026d54efdc8fc105fedf98d65e173d8.mugqic.done
)
macs2_callpeak_23_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_24_JOB_ID: macs2_callpeak_bigBed.Rbp1-P-Ser2_SHP1-KO
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Rbp1-P-Ser2_SHP1-KO
JOB_DEPENDENCIES=$macs2_callpeak_23_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Rbp1-P-Ser2_SHP1-KO.4f8d92e563c7a98567a04abf214d7d64.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Rbp1-P-Ser2_SHP1-KO.4f8d92e563c7a98567a04abf214d7d64.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO_peaks.narrowPeak > peak_call/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/Rbp1-P-Ser2_SHP1-KO/Rbp1-P-Ser2_SHP1-KO_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Rbp1-P-Ser2_SHP1-KO.4f8d92e563c7a98567a04abf214d7d64.mugqic.done
)
macs2_callpeak_24_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_25_JOB_ID: macs2_callpeak_report
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_report
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID:$macs2_callpeak_3_JOB_ID:$macs2_callpeak_5_JOB_ID:$macs2_callpeak_7_JOB_ID:$macs2_callpeak_9_JOB_ID:$macs2_callpeak_11_JOB_ID:$macs2_callpeak_13_JOB_ID:$macs2_callpeak_15_JOB_ID:$macs2_callpeak_17_JOB_ID:$macs2_callpeak_19_JOB_ID:$macs2_callpeak_21_JOB_ID:$macs2_callpeak_23_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_report.004cc8a438bf86e6773926dee6e95e9d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_report.004cc8a438bf86e6773926dee6e95e9d.mugqic.done'
mkdir -p report && \
cp /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.4/bfx/report/ChipSeq.macs2_callpeak.md report/ && \
for contrast in SHP1_WT SHP1_SHP1-KO Rbp1_WT Rbp1_SHP1-KO Rbp1-P-Ser2_WT Rbp1-P-Ser2_SHP1-KO SHP1_WT SHP1_SHP1-KO Rbp1_WT Rbp1_SHP1-KO Rbp1-P-Ser2_WT Rbp1-P-Ser2_SHP1-KO
do
  cp -a --parents peak_call/$contrast/ report/ && \
  echo -e "* [Peak Calls File for Design $contrast](peak_call/$contrast/${contrast}_peaks.xls)" \
  >> report/ChipSeq.macs2_callpeak.md
done
macs2_callpeak_report.004cc8a438bf86e6773926dee6e95e9d.mugqic.done
)
macs2_callpeak_25_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
LOG_MD5=$(echo $USER-'206.12.124.2-ChipSeq-Rbp1-P-Ser2_SHP1-KO.MS8_S8_L001_R1_001.fastq.gz.MS8_S8_L002_R1_001.fastq.gz,SHP1_WT.MS3_S3_L001_R1_001.fastq.gz.MS3_S3_L002_R1_001.fastq.gz,WCE_SHP1-KO.MS2_S2_L001_R1_001.fastq.gz.MS2_S2_L002_R1_001.fastq.gz,Rbp1_SHP1-KO.MS6_S6_L001_R1_001.fastq.gz.MS6_S6_L002_R1_001.fastq.gz,Rbp1_WT.MS5_S5_L001_R1_001.fastq.gz.MS5_S5_L002_R1_001.fastq.gz,Rbp1-P-Ser2_WT.MS7_S7_L001_R1_001.fastq.gz.MS7_S7_L002_R1_001.fastq.gz,WCE_WT.MS1_S1_L001_R1_001.fastq.gz.MS1_S1_L002_R1_001.fastq.gz,SHP1_SHP1-KO.MS4_S4_L001_R1_001.fastq.gz.MS4_S4_L002_R1_001.fastq.gz' | md5sum | awk '{ print $1 }')
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=cedar1.cedar.computecanada.ca&ip=206.12.124.2&pipeline=ChipSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,bwa_mem_picard_sort_sam,samtools_view_filter,picard_merge_sam_files,picard_mark_duplicates,macs2_callpeak&samples=8&md5=$LOG_MD5" --quiet --output-document=/dev/null

